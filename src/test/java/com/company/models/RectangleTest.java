package com.company.models;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class RectangleTest {
    List<Rectangle> rectangleList = new ArrayList<>();
    final Rectangle baseRect;
    Rectangle rect1;
    Rectangle rect2;
    Rectangle rect3;
    Rectangle rect4;
    Rectangle rect5;
    Rectangle rect6;
    Rectangle rect7;
    Rectangle rect8;

    public RectangleTest() {
        baseRect = new Rectangle("BASE", new Position(5,1), new Position(2,3));

        rect1 = new Rectangle("Rectangle 1", new Position(2,1), new Position(1,3));
        rect2 = new Rectangle("Rectangle 2", new Position(5,5), new Position(1,6));
        rect3 = new Rectangle("Rectangle 3", new Position(3,4), new Position(2,5));
        rect4 = new Rectangle("Rectangle 4", new Position(5,1), new Position(3,2));
        rect5 = new Rectangle("Rectangle 5", new Position(7,2), new Position(4,3));
        rect6 = new Rectangle("Rectangle 6", new Position(8,3), new Position(7,6));
        rect7 = new Rectangle("Rectangle 7", new Position(10,2), new Position(8,3));
        rect8 = new Rectangle("Rectangle 8", new Position(9,1), new Position(6,4));

        rectangleList.add(rect1);
        rectangleList.add(rect2);
        rectangleList.add(rect3);
        rectangleList.add(rect4);
        rectangleList.add(rect5);
        rectangleList.add(rect6);
        rectangleList.add(rect7);
        rectangleList.add(rect8);
    }

    @Test
    public void intersect() {
        rectangleList.forEach(rectangle -> {
            System.out.println(rectangle.toString());
            if (rectangle.intersect(baseRect)) {
                System.out.println("Rectangles intersect.");
            } else {
                System.out.println("Rectangles do not intersect.");
            }
            System.out.println("====================================================");
        });
    }
}