package com.company;

import com.company.models.Position;
import com.company.models.Rectangle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String[] coords;

        System.out.println("Enter coordinates of the first Rectangle: ");
        // First Rectangle
        System.out.println("Enter x,y for the bottom-right coordinate: ");
        coords = bufferedReader.readLine().split(",");
        Position bottomRight1 = new Position();
        bottomRight1.setX(Integer.parseInt(coords[0]));
        bottomRight1.setY(Integer.parseInt(coords[1]));

        System.out.println("Enter x,y for the top-left coordinate: ");
        coords = bufferedReader.readLine().split(",");
        Position topLeft1 = new Position();
        topLeft1.setX(Integer.parseInt(coords[0]));
        topLeft1.setY(Integer.parseInt(coords[1]));

        System.out.println("Enter coordinates of the second Rectangle: ");
        // Second Rectangle
        System.out.println("Enter x,y for the bottom-right coordinate: ");
        coords = bufferedReader.readLine().split(",");
        Position bottomRight2 = new Position();
        bottomRight2.setX(Integer.parseInt(coords[0]));
        bottomRight2.setY(Integer.parseInt(coords[1]));

        System.out.println("Enter x,y for the top-left coordinate: ");
        coords = bufferedReader.readLine().split(",");
        Position topLeft2 = new Position();
        topLeft2.setX(Integer.parseInt(coords[0]));
        topLeft2.setY(Integer.parseInt(coords[1]));

        Rectangle r1 = new Rectangle(bottomRight1,topLeft1);
        Rectangle r2 = new Rectangle(bottomRight2, topLeft2);
        System.out.println(r1.intersect(r2));
    }
}
