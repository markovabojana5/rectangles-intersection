package com.company.models;

public class Position {
    private int x;
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position() {}

    public int getX() {
        return x;
    }

    public void setX(int x) {
        if(x < 0) {
            throw new IllegalArgumentException("Only positive numbers allowed!");
        }
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        if(y < 0) {
            throw new IllegalArgumentException("Only positive numbers allowed!");
        }
        this.y = y;
    }

    @Override
    public String toString() {
        return "{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
