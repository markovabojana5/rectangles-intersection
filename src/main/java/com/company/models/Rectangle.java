package com.company.models;

public class Rectangle {
    public String name;
    public Position bottomRight;
    public Position topLeft;

    public static int counter = 1;

    public Rectangle(String name, Position bottomRight, Position topLeft) {
        this.name = name;
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Rectangle(Position bottomRight, Position topLeft) {
        this.name = String.format("Rectangle %d", counter++);
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public boolean intersect(Rectangle r2) {
        return !(this.bottomRight.getX() <= r2.topLeft.getX() || this.bottomRight.getY() >= r2.topLeft.getY()
                || this.topLeft.getX() >= r2.bottomRight.getX() || this.topLeft.getY() <= r2.bottomRight.getY());
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "name=" + name +
                ", bottomRight=" + bottomRight +
                ", topLeft=" + topLeft +
                '}';
    }
}
